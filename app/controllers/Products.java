package controllers;

import play.mvc.*;
import views.html.products.list;
import views.html.products.details;

import play.data.Form;

import java.util.List;
import java.util.*;
import models.Product;
import models.Tag;

import com.avaje.ebean.Ebean;

public class Products extends Controller{
	
	public static Result list(){
		List<Product> products = Product.findAll();
		return ok(list.render(products));
	}
	
	//Form begin
	private static final Form<Product> productForm = Form.form(Product.class);
	public static Result newProduct(){
		return ok(details.render(productForm));
	}
	//From end
	
	public static Result details(Product product) {
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	
	public static Result delete(String ean) {
		final Product product = Product.findByEan(ean);
		if(product == null) {
			return notFound(String.format("Product %s does not exists.", ean));
		}
		product.delete();
		return redirect(routes.Products.list());
	}
	
	public static Result save() {
		Form<Product> boundForm = productForm.bindFromRequest();
		
		if (boundForm.hasErrors()) {						//Thong bao ve xu ly khong dien thong tin
		flash("error", "Please correct the form below.");	//flash : no dc xu ly o main.scala.html
		return badRequest(details.render(boundForm));
		}
		
		Product product = boundForm.get();			
		List<Tag> tags = new ArrayList<Tag>();		// Tag : no xuly Tag
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;						// end Handling Tag
		Ebean.save(product);							
		
		flash("success", String.format("Successfully added product %s", product));	//Thong bao ve xu ly khong dien thong tin.
		return redirect(routes.Products.list());
	}
	
	
}










